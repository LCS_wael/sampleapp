@extends('layouts.app')

@section('content')
    <h1>Home</h1>
    <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex, ipsam, non? Accusamus asperiores blanditiis cum deserunt incidunt inventore odit perferendis quisquam ut voluptate. Amet blanditiis eos, esse harum repudiandae ullam.</p>
@endsection

@section('sidebar')
    @parent
    <p>home side bar</p>
@endsection
